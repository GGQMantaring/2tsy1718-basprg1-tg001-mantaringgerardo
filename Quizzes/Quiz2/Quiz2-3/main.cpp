#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
	int min, max, rng, dmg, fnum;

	//Min Max Range Inputs

	cout << "You are facing a kobold. You gear up for an attack, but" << endl << "your 'Sword of User Input Damage' forgets how strong it is." << endl << "You need to remind it of just how much damage it can do." << endl;
	system("pause");
	system("cls");
	cout << "Minimum Damage: ";
	cin >> min;
	cout << "Maximum Damage: ";
	cin >> max;
	cout << "All right. Your sword is doing a few calculations." << endl;
	system("pause");
	system("cls");

	//Min Max Range Calculation and Display
	rng = max - min;
	dmg = rand() % min + rng;

	cout << "You take a swing." << endl << "Your sword does " << dmg << " to the beast!" << endl << "It looks kinda dead, so I guess you win." << endl << "Yay!~" << endl;
	system("pause");
return 0;
}