#include <iostream>

int main()
{
	float a;
	float b;
	float c;
	float d;
	float e;
	float ans;
	
	//Input
	std::cout << "I'm going to ask you for five numbers. In a while, I'll begin computing for their sum and average" << std::endl;
	system("pause");
	system("cls");
	std::cout << "AT THE SAME TIME!!" << std::endl;
	system("pause");
	system("cls");
	std::cout << "Let's do this . . ." << std::endl;
	system("pause");
	system("cls");
	
	std::cout << "Give me those five numbers now! " << std::endl;
	std::cin >> a;
	std::cin >> b;
	std::cout << "Don't worry, just keep going." << std::endl;
	std::cin >> c;
	std::cin >> d;
	std::cout << "One more!" << std::endl;
	std::cin >> e;
	std::cout << "Ok, those look promising. Let's see here . . ." << std::endl;
	system("pause");
	system("cls");

	//Calulations with Results
	ans = a + b + c + d + e;
	std::cout << "Okay, so the sum of " << a << ", " << b << ", " << c << ", " << d << ", and " << e << " is " << ans << "... " << std::endl;
	ans = ans / 5;
	std::cout << "and as for their average, that would be as easy as " << ans << "!" << std::endl;

	//Outro
	system("pause");
	system("cls");
	std::cout << "Well, that was fun! I hope you run me again." << std::endl;

	system("pause");
	return 0;
}