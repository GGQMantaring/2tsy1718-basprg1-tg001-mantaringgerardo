#include <iostream>

int main()
{
	float cArea;
	float cCirc;
	float rad;

	//Input
	std::cout << "Let's get the area and circumference of a circle!" << std::endl << "Try measuring for the radius of your circle!" << std::endl;
	system("pause");
	system("cls");
	std::cout << "You were able to get it?" << std::endl << "Ok, now what was its radius?" << std::endl << "Radius: ";
	std::cin >> rad;
	system("cls");

	//Calulations
	cArea = 3.14 * rad * rad;
	cCirc = 6.28 * rad;

	//Results
	std::cout << "With a radius of " << rad << ", your circle has a circumference of " << cCirc << " units long, and covers an area of " << cArea << " units squared." << std::endl;
		
system("pause");
	return 0;
}