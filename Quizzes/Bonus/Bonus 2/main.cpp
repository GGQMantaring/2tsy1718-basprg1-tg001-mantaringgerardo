#include <iostream>

using namespace std;

int main()
{
	int num1 = 10;
	int num2 = 15;

	cout << "BEFORE" << endl;
	cout << "num1 = " << num1 << endl;
	cout << "num2 = " << num2 << endl << endl;

	num1 = num2 - num1;
	num2 = num1;
	num2 = num2 + num1;
	num1 = (num2 - num1) + num2;

	cout << "AFTER" << endl;
	cout << "num1 = " << num1 << endl;
	cout << "num2 = " << num2 << endl;

	system("pause");
	return 0;
}