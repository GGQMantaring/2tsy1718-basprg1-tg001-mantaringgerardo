#include <iostream>

using namespace std;

int main()
{
	int secs, h, m, s;

	cout << "Give me the time, except make it in terms of seconds!  ";
	cin >> secs;
	cout << endl << "Okay, cool. Now, watch me convert it!" << endl << endl;
	system("pause");
	system("cls");

	h = secs / 360;
	m = (secs % 360) / 60;
	s = secs % 60;

	cout << "The converted time is: " << h << "H " << m << "M " << s << "S." << endl << endl;
	cout << "Thank you for using me. Use me again!" << endl << endl;
	system("pause");
	return 0;
}