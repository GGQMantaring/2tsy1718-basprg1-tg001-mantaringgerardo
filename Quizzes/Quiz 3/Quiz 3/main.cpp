#include <iostream>

using namespace std;

int main()
{
	int counter = 1, counterOrig, peg;

	//Input triangle height
	cout << "Let's draw a triangle!" << endl << "How tall do you want this triangle to be?" << endl << endl;
	cout << "Input Height: ";
	cin >> counterOrig;
	cout << endl;
	system("pause");
	system("cls");

	//Printing magic
	while (counter < counterOrig + 1)
	{
		for (peg = 0; peg != counter; peg++)
		{
			cout << "*";
		}
		counter++;
		cout << endl;
	}

	cout << endl;
	cout << "That's nice. Thanks for playing!" << endl << endl;
	
	system("pause");
	return 0;
}