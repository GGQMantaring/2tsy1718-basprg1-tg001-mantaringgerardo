#include <iostream>

using namespace std;

int main()
{
	int prev = 0, prevOrig, curr = 1, limit;

	//Input
	cout << "Hey, Mr. Fib-yo-nacho wants your phone number, baby. Give him a number he won't forget!" << endl << endl;
	system("pause");
	system("cls");
	cout << "Actually, I'm just kidding, this sounds really sketchy and you should give a fake number instead." << endl;
	cout << "Give me a number and I'll generate the Fibonacci sequence up to that many that you mention." << endl << endl;
	cout << "Input number:  ";
	cin >> limit;
	cout << endl << endl << "Solid. Give me a second. . ." << endl << endl;
	system("pause");
	system("cls");

	//Print Loop
	for (size_t i = 0; i < limit; i++)
	{
		cout << curr;
		
		//Punctuation
		if (i < limit - 1)
		{
			cout << ", ";
		}
		else if (i == limit - 1)
		{
			cout << ".";
		}

		//Calculation
		prevOrig = prev;

		curr = prevOrig + curr;
		prev = curr - prevOrig;
		
	}

	cout << endl << endl << "All right. You gave him that number and hopefully he's too much of a chad to know the difference." << endl;
	cout << "Stranger danger avoided, good work. Stay safe and see you next time.";
	system("pause");
	return 0;
}