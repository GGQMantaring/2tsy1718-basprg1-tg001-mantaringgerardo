#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
	int randX, randY, counter = 0;

	//Intro
	cout << "Let's roll dice until we can get a multiple of 4!" << endl << endl;

	//Rolling 2d6
	do
	{
		
		//Die X
		srand(time(NULL));
		randX = rand() % 6 + 1;
		cout << randX << ", ";
		

		//Die Y
		randY = rand() % 6 + 1;
		cout << randY << endl;
		counter++;
	} while ((randX + randY) % 4 != 0);

	cout << "You rolled " << counter << " times." << endl;

	system("pause");
	return 0;
}