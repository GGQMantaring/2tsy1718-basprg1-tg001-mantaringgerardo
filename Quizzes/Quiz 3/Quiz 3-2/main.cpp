#include <iostream>

using namespace std;

int main()
{
	int vertSwitch = 1, horSwitch = 1;


	for (size_t i = 0; i < 6; i++)
	{
		for (size_t i = 0; i < 8; i++)
		{
			if (horSwitch == 1)
			{
				cout << "X";
				horSwitch--;
			}
			else
			{
				cout << "O";
				horSwitch++;
			}
		}
		cout << endl;

		if (vertSwitch == 1)
		{
			horSwitch = 0;
			vertSwitch++;
		}
		else
		{
			horSwitch = 1;
			vertSwitch--;
		}

	}

	cout << endl;

	system("pause");
	return 0;
}