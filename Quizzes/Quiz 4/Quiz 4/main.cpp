#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	int x, numbers[100], val = 1;

	//Filling the array
	for (x = 0; x < 100; x++)
	{
		numbers[x] = val++;
	}

	//Randomization of missing number
	srand(time(NULL));
	x = rand() % 100;
	numbers[x] = 0;

	//Flavour
	cout << "I've taken one number from between 1-100 at random and removed it from this list. Can you find it?" << endl << endl;
	system("pause");
	system("cls");

	//Printing the array
	for (x = 0; x < 100; x++)
	{
		if (numbers[x] == 0) 
		{
		}

		else
		{
			cout << numbers[x] << endl;
		}
	}

	cout << endl;
	cout << "Did you guess which one it was? It's pretty hard actually, since you have a 1% chance of getting it right. . ." << endl << endl;
	cout << "The missing number is actually ";

	//Determining the missing number
	val = 1;
	for (x = 0; x < 100; x++)
	{
		if(numbers[x] != val)
		{
			cout << val << ". Did you guess it right?" << endl << endl;
		}

		val++;
	}
	
	system("pause");
	return 0;
}