#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	int x, y, numbers[20], val, large[2]{ 0,0 }, dupe[20];

	srand(time(NULL));

	cout << "Here's an array of 20 items, with each item being randomly pulled from 1-100:" << endl << endl;
	
	//Q4-3A
	//Printing the array
	for (x = 0; x < 20; x++)
	{
		//Randomization of value
		val = rand() % 100 + 1;
		numbers[x] = val;
		cout << numbers[x];
		
		if (x < 19)
		{
			cout << ", ";
		}
		else if (x == 19)
		{
			cout << "." << endl << endl;
		}
	}

	//Q4-3B
	//Calculating the two largest numbers
	for (x = 0; x < 20; x++)
	{
		if (numbers[x] > large[0])
		{
			large[0] = numbers[x];
		}
	}

	for (x = 0; x < 20; x++)
	{
		if (numbers[x] > large[1] && numbers[x] < large[0])
		{
			large[1] = numbers[x];
		}
	}
	//Printing the two largest numbers
	cout << "And the two largest of this group are: " << large[0] << " and " << large[1] << "." << endl << endl;

	//Q4-3C
	//Convoluted process of duplicating the array
	for (x = 0; x < 20; x++)
	{
		dupe[x] = numbers[x];
	}

	//Even more convoluted way of checking for duplicates
	for (x = 0; x < 20; x++)
	{
		for (y = x; y < 20; y++)
		{
			if (y != x)
			{
				if (dupe[y] == numbers[x])
				{
					cout << "And " << dupe[y] << " is a duplicate." << endl;
				}
			}
		}
	}

	cout << endl;
	system("pause");
	return 0;
}