#include <iostream>
#include <string>

using namespace std;

int main()
{

	float cHP;
	float mHP;
	string status;
	
	//Input
	cout << "Enter your max health points." << endl;
	cout << "Max HP: ";
	cin >> mHP;
	cout << "Enter your current health points." << endl << "Current HP: ";
	cin >> cHP;
	system("cls");
	cout << "Let's see how you're doing . . ." << endl;
	system("pause");
	system("cls");

	//Computation of Parameters
	if (cHP / mHP == 1) {
		status = "Full";
	}
	if (cHP / mHP > .4999 && cHP / mHP <= .9999) {
		status = "Green (Healthy)";
	}
	else if (cHP / mHP > .1999 && cHP / mHP < .5) {
		status = "Yellow (Warning)";
	}
	else  if (cHP / mHP > 0 && cHP / mHP < .2) {
		status = "Red (Critical)";
	}
	else if (cHP <= 0) {
		status = "Dead";
	}

	//Results
	cout << "Here's how you're doing: " << endl << status << endl;
		system("pause");
	return 0;
}