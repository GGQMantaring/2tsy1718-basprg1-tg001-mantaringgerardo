#include <iostream>

using namespace std;

int main()
{

	float x;
	float y;
	int ans;
	
	//Input
	cout << "Enter values for X and Y: " << endl;
	cout << "X: ";
	cin >> x;
	cout << "Y: ";
	cin >> y;
	system("cls");
	cout << "Now comparing values..." << endl;
	system("pause");
	system("cls");

	//Results
	ans = x == y;
	cout << x << " == " << y << " = " << ans << endl;
	ans = x != y;
	cout << x << " != " << y << " = " << ans << endl;
	ans = x > y;
	cout << x << " > " << y << " = " << ans << endl;
	ans = x < y;
	cout << x << " < " << y << " = " << ans << endl;
	ans = x >= y;
	cout << x << " >= " << y << " = " << ans << endl;
	ans = x <= y;
	cout << x << " <= " << y << " = " << ans << endl;
	
	system("pause");
	return 0;
}