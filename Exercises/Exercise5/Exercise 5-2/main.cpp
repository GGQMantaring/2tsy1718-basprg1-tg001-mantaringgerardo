#include <iostream>
#include <string>

using namespace std;

int main()
{
	int valOrig, valComp = 0, valQuan = 12;
	string adj, adj2;

	//No Negatives Pls
	//Not Even The C++ Gods Can Count This High
	do
	{
		if (valQuan < 13 && valQuan >= 0)
		{
			cout << "Give me a number and I'll get its factorial. No negatives, okay?" << endl;
			cout << "Seriously, don't try it." << endl;
			cout << "And don't give me a number higher than 12, please." << endl;
		}
		else if (valQuan < 0)
		{
			cout << "I have no space in my virtual life for that sort of negativity." << endl;
			cout << "Try again, buckaroo." << endl << endl;
		}
		else
		{
			cout << "Woah, nelly! That's too big! I haven't figured out how to make sure calculations past 13! are" << endl;
			cout << "computed properly. Let's try a smaller number where the answer won't bug out. . ." << endl << endl;
		}

		cout << "Factorial of: ";
		cin >> valQuan;
		cout << endl;
		cout << "Okay, give me a second." << endl << endl;
		system("pause");
		system("cls");
	} while (valQuan < 0 || valQuan > 12);

	valOrig = valQuan;

	//Cheating, probably
	if (valQuan == 0)
	{
		cout << "Oh, that's easy" << endl;
		cout << "0! = 1" << endl << endl;
		system("pause");
		return 0;
	}

	//Computation
	for (valComp = 1; valQuan != 1; valQuan--)
	{
		valComp = valComp * valQuan;
	}

	if (valOrig < 6)
	{
		adj = "weak";
		adj2 = "Do you even lift, bruh?";
	}
	else
	{
		adj = "swole";
		adj2 = "That number's jacked, man!";
	}
	cout << "Your number is " << adj << "." << endl << endl;
	cout << "Basically, " << valOrig << "! is exactly " << valComp << "." << endl << endl;
	cout << adj2 << endl << endl;
	system("pause");
	return 0;
}