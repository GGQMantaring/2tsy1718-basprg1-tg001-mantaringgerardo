#include <iostream>

using namespace std;

int main()
{
	int countSOrig = 0, countS = 0, countG = 5, goal = 5;

	//Magic
	while (goal > 1)
	{
		goal = countG;
		countSOrig = countS;

		while (countS != 0)
		{
			cout << " ";
			countS--;
		}
		while (countG != 0)
		{
			cout << "*";
			countG--;
		}

		cout << endl;
		countS = countSOrig + 1;
		countG = goal - 2;
	}

	countG = 3;
	countS = 1;

	while (goal != 5)
	{
		goal = countG;
		countSOrig = countS;

		while (countS != 0)
		{
			cout << " ";
			countS--;
		}
		while (countG != 0)
		{
			cout << "*";
			countG--;
		}

		countS = countSOrig - 1;
		countG = goal + 2;
		cout << endl;

	}
	
	system("pause");
	return 0;
}