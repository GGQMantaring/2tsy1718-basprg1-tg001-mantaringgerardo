#include <iostream>

using namespace std;

int main()
{
	int countSOrig, countS = 3, countG = 1, goal;

	countSOrig = countS;
	do
	{		
		goal = countG;
		countSOrig = countS;

		while (countS != 0)
		{
			cout << " ";
			countS--;
		}
		while (countG != 0)
		{
			cout << "*";
			countG--;
		}

		countS = countSOrig - 1;
		countG = goal + 2;
		cout << endl;

	} while (goal != 7);
	
	cout << endl << endl;
	system("pause");
	return 0;
}