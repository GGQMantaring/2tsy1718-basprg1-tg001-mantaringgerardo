#include <iostream>

using namespace std;

int main()
{
	int counter, goal = 5;

	//It's just the reverse of 5-5
	while (goal >= 0)
	{
		for (counter = goal; counter != 0; counter--)
		{
			cout << "*";
		}
		goal--;
		cout << endl;
	}

	cout << endl << endl;
	system("pause");
	return 0;
}