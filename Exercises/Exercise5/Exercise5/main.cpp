#include <iostream>

using namespace std;

int main()
{
	int val, valComp = 0, valCount = 0, valQuan;

	cout << "How many numbers do you want me to average? ";
	cin >> valQuan;
	cout << endl << endl;
	cout << "Okay, give me a second." << endl;
	system("pause");
	system("cls");

	//Tampo Section
	if (valQuan == 0) 
	{
		cout << "Oh, you didn't want to evaluate numbers with me?" << endl;
		cout << "FINE THEN" << endl << endl;
		system("pause");
		return 0;
	}

	//Actual Thing
	for ( valCount = 1 ; valCount != valQuan + 1 ; valCount++ ) 
	{
		cout << "Input Value " << valCount << ": ";
		cin >> val;
		cout << endl << endl;
		system("cls");

		valComp = valComp + val; //Calculation
	}

	valComp = valComp / valQuan; //Averaging

	cout << "Ok, it took me a while, but I finally did it." << endl << endl;
	cout << "Just kidding. I'm a computer." << endl;
	cout << "I literally did it faster than you can even imagine me doing it." << endl << endl;
	cout << "The average of the numbers you typed out is " << valComp << "." << endl << endl;
	system("pause");
	return 0;
}