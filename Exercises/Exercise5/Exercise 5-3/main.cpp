#include <iostream>

using namespace std;

int main()
{
	int valOrig, valLast, valCount= 1, valTerms = 1, valDiff, valAns;

	//Input Values
	
	cout << "We're gonna do Arithmetic Progressions, and hoo boy I am not that excited for this." << endl;
	cout << "Not that I'm worried about calculating them, because I'm an AI with lightning fast computing skill," << endl;
	cout << "but because the guy coding this is the one not excited, and he's projecting his own apprehension" << endl;
	cout << "though my simulated personality--an echo of his creative output, the product of his late night" << endl;
	cout << "coding session, the vessel which carries the fruit of his work, the mother of this exercise's grade." << endl << endl;
	cout << "So let's do this." << endl << endl;
	system("pause");
	system("cls");

	while (valTerms < 2)
	{
		cout << "How many numbers are we going to add together?  ";
		cin >> valTerms;
		cout << endl << "Now, what is the difference between each number?  ";
		cin >> valDiff;
		cout << endl << "And lastly, which lucky number do we start this progression from?  ";
		cin >> valOrig;
		cout << endl << endl;
		system("pause");
		system("cls");

		if (valTerms < 2)
		{
			cout << endl;
			cout << "Those are not nearly enough terms. Input a new number for that field." << endl << endl;
			system("pause");
			system("cls");
		}

	}

	//Calculations

	valLast = valOrig;

	do
	{
		valLast = valLast + valDiff;
		valCount++;
	} while (valCount != valTerms);

	valAns = ((((valLast - valOrig) / valDiff) + 1)*(valOrig + valLast)) / 2;

	//Fingers Crossed
	cout << "Starting from " << valOrig << ", with a linear progression of " << valDiff << " steps, the sum of" << endl;
	cout << "the arithmetic progression is " << valAns << "." << endl << endl;
	system("pause");
	return 0;
}