#include <iostream>

using namespace std;

int main()
{
	int counter, goal = 1;

	//This is where the magic happens
	while (goal <= 5)
	{
		for (counter = goal; counter != 0; counter--)
		{
			cout << "*";
		}
		goal++;
		cout << endl;
	}						//Can I just say, sir. I feel proud I figured this one out.

	cout << endl << endl;
	system("pause");
	return 0;
}