#include <iostream>
#include <string>

int main()
{

	std::string Fname;
	std::string Lname;
	int age;
	int cashola;
	std::string bfast;

	//Prelude
	std::cout << "Hi. This is your personal Wellness Checkup Interface." << std::endl;
	std::cout << "Please answer a few questions so we may evaluate your current Wellness levels." << std::endl;
	system("pause");
	system("cls");

	//Fname
	std::cout << "What is your first name? ";
	std::cin >> Fname;
	std::cout << "Oh, that's a nice name." << std::endl;
	system("pause");
	system("cls");

	//Lname
	std::cout << "What is your last name? ";
	std::cin >> Lname;
	std::cout << "Excellent." << std::endl;
	system("pause");
	system("cls");

	//Age
	std::cout << "Now, if you don't mind me asking, how old are you? ";
	std::cin >> age;
	std::cout << "That's not too old, hahaha." << std::endl;
	system("pause");
	system("cls");

	//cashola
	std::cout << "And how much money do you have on you right now? ";
	std::cin >> cashola;
	std::cout << "Nice. Noted..." << std::endl;
	system("pause");
	system("cls");

	//bfast
	std::cout << "Finally, yes or no... have you eaten breakfast today? ";
	std::cin >> bfast;
	std::cout << "Yes, yes. Interesting..." << std::endl;
	system("pause");
	system("cls");

	std::cout << "Thank you for answering those questions." << std::endl;
	std::cout << "It'll take a moment to process your information." << std::endl;
	system("pause");
	system("cls");

	//Printing
	std::cout << "Name: " << Fname << " " << Lname << std::endl;
	std::cout << "Age: " << age << std::endl;
	std::cout << "Money: " << cashola << std::endl;
	std::cout << "Breakfast Status: '" << bfast << "'" << std::endl;

	system("pause");
	return 0;
}