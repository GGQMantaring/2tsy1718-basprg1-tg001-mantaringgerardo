#include <iostream>
#include <string>

int main()
{
	float usd;
	float rate;
	float php;

	//Input
	std::cout << "How much money do you want changed?" << std::endl;
	std::cout << "$";
	std::cin >> usd;
	std::cout << "Calculating . . .";
	rate = 51.88;
	php = usd * rate;
	system("pause");
	system("cls");

	//Results
	std::cout << "Php " << php << std::endl;

	system("pause");
	return 0;
}