#include <iostream>
#include <string>

int main()
{

	int xval;
	int yval;
	int add;
	int sub;
	int mult;
	int div;
	float mod;

	//Input
	std::cout << "Please input whole number values for x and y." << std::endl;
	std::cout << "X: ";
	std::cin >> xval;
	std::cout << "Y: ";
	std::cin >> yval;
	system("pause");
	std::cout << "Calculating . . ." << std::endl;
	system("pause");
	system("cls");

	//Operation
	add = xval + yval;
	sub = xval - yval;
	mult = xval  * yval;
	div = xval / yval;
	mod = xval % yval;

	//Results
	std::cout << "x + y = " << add << std::endl;
	std::cout << "x - y = " << sub << std::endl;
	std::cout << "x * y = " << mult << std::endl;
	std::cout << "x / y = " << div << std::endl;
	std::cout << "x % y = " << mod << std::endl;



	system("pause");
	return 0;
}