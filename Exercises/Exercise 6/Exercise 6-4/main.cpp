#include <iostream>
#include <string>

using namespace std;

int main()
{
	int x = 0;
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	string answer;

	cout << R"_(Bokun looks at you from behind the counter. "You lookin' for your things?")_" << endl << endl;
	cout << R"_("Yeah. I want my stuff back.")_" << endl << endl;
	cout << R"_("Okay, I'll tell you where your things are, but you're gonna have to be specific")_" << endl << endl;

	while (0 == 0)
	{
		cout << R"_("What are you looking for?" -> )_";
		cin >> answer;

		for (x = 0; x < 8; x++)
		{
			if (answer == items[x])
			{
				cout << answer << "Your item is located in array index number " << x << "." << endl << endl;
				system("pause");
				return 0;
			}
		}

		cout << answer << " does not exist." << endl << endl;
	}

	system("pause");
	return 0;
}