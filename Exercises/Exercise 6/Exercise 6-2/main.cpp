#include <iostream>

using namespace std;

int main()
{
	cout << "Let's add two arrays together!" << endl << "Whatever." << endl << endl;

	int x;
	int numbers1[] = { 1, 2, 3, 4, 5 };
	int numbers2[] = { 0, 7, -10, 5, 3 };
	int numbers3[5];

	for (x = 0; x < 5; x++)
	{
		numbers3[x] = numbers1[x] + numbers2[x];
	}

	for (x = 0; x < 5; x++)
	{
		cout << numbers3[x] << endl;
	}

	cout << endl;
	system("pause");
	return 0;
}