#include <iostream>
#include <string>

using namespace std;

int main()
{
	int x = 0, counter = 0;
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	string answer;

	cout << R"_(Bokun looks at you from behind the counter. "You lookin' for your things?")_" << endl << endl;
	cout << R"_("Yeah. I want to know how much of an item I have with you.")_" << endl << endl;
	cout << R"_("Okay, but you're gonna have to be specific")_" << endl << endl;

	while (0 == 0)
	{
		cout << R"_("What are you looking for?" -> )_";
		cin >> answer;

		for (x = 0; x < 8; x++)
		{
			if (answer == items[x])
			{
				counter++;
			}
		}

		if (counter == 0)
		{
			cout << R"_("Huh, you don't have any of that item. Wanna try again?")_" << endl << endl;
		}

		else
		{
			cout << R"_("You have )_" << counter << " " << answer << R"_((s) with me.")_" << endl << endl;
			cout << R"_("Okay, cool. Can I have them back?")_" << endl << endl;
			cout << R"_("No.")_" << endl << endl;
			system("pause");
			return 0;
		}
	}
}