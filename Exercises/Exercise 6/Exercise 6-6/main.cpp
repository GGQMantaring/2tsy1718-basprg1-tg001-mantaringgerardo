#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int main()
{
	int RandIndex;
	string input;
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };

	cout << "Press Z to randomly pull an item from your inventory." << endl;
	cout << "Press X to close the program." << endl << endl;

	while (true)
	{
		cout << "Input: ";
		cin >> input;
		cout << endl;

		if (input == "z" || input == "Z")
		{
			srand(time(NULL));
			RandIndex = rand() % 8;
			cout << items[RandIndex] << endl << endl;
		}		

		else if (input == "x" || input == "X")
		{
			break;
		}
	} 

	return 0;
}