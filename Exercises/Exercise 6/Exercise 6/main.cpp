#include <iostream>
#include <string>

using namespace std;

int main()
{
	int x;
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };

	cout << R"_(The goblin cop looks at you in disdain. "Look, you've got quite a lot of stuff that's considered contraband.")_" << endl;
	cout << R"_(He starts pulling things out of your pants, which he had confiscated a few seconds ago. "Let's see here...")_" << endl << endl;
	cout << R"_("You have:)_" << endl << endl;

	for (x = 0; x < 8; x++)
	{
		cout << items[x] << endl;
	}
	
	cout << endl << endl << R"_(... and that's about it." He takes all the things and shoves it into a sack the size of a person.)_" << endl;
	cout << "He then pulls out another sack and stuffs you into it. Good job getting arrested by the Kar'zhadun Police Dep't." << endl << endl;
	system("pause");
	return 0;
}