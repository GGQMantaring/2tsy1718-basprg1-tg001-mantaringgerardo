#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
	string name, foeName, Atk, foeAtk;
	string adj[7] = { "strong", "funny", "worthy", "stupid", "nice", "very unfortunate", "unique" };
	string foe[8] = { "a baby seal", "a jar of strawberry jellyfish", "a seagull", "a coral (yes, a coral is an animal)", "a fish with legs", "a lost diver", "The Elder God Cthulhu's unborn child", "the air, which is apparently sentient and extremely hostile" };
	int HP, HPmax = 0, Min, Max;						//Player and enemy health is set to 0 to set up the loop below w/c is dependent on both player and AI as 'alive'
	int foeHP, foeHPmax = 0, foeMin, foeMax;
	int Dmg, foeDmg, foeAI;
	int RandIndex;

	//Prologue
	cout << R"_(    ___                           /\        _  _                _       )_" << endl;		//Completely unnecessary, but I loved including this
	cout << R"_(   F __".   ____    _    _       |  |      FJ / ;     ____     FJ___    )_" << endl;
	cout << R"_(  J (___|  F __ J  J |  | L      ||||     J |/ (|    F __ J   J  __ `.  )_" << endl;
	cout << R"_(  J\___ \ | |--| | | |  | |      ||||     |     L   | |--| |  | |--| |  )_" << endl;
	cout << R"_( .--___) \F L__J J F L__J J      ||||     F L:\  L  F L__J J  F L  J J  )_" << endl;
	cout << R"_( J\______J\______/F)-____  L   O-=/\=-O  J__L \\__LJ\______/FJ__L  J__L )_" << endl;
	cout << R"_(  J______FJ______FJ\______/F      ||     |__L  \L_| J______F |__L  J__| )_" << endl;
	cout << R"_(                   J______F      <__>                                   )_" << endl;
	cout << R"_(    ___                                 ___                        __   )_" << endl;
	cout << R"_(   F __".    ____     _ ___     __     F __".    ____      ____    LJ   )_" << endl;
	cout << R"_(  J |--\ L  F __ J   J '__ J    LJ    J |--\ L  F __ J    F __ J   FJ   )_" << endl;
	cout << R"_(  | |  J | | _____J  | |__| |   --    | |  J | | |--| |  | |--| | J  L  )_" << endl;
	cout << R"_(  F L__J | F L___--. F L  J J   __    F L__J | F L__J J  F L__J J J  L  )_" << endl;
	cout << R"_( J______/FJ\______/FJ__L  J__L J__L  J______/FJ\______/FJ\______/FJ__L  )_" << endl;
	cout << R"_( |______F  J______F |__L  J__| |__|  |______F  J______F  J______F |__|	by Jero)_" << endl;
	cout << endl;
	cout << "~ Welcome to Soy Koh Den: Dool! ~" << endl << "Tell me, chosen protagonist. What is your name?" << endl << "Name: ";
	cin >> name;
	system("cls");
	cout << endl << endl << endl;

	srand(time(NULL));				//Random Seed Initialization; I searched on how to do this, because I didn't want to use several if/else if layers 
	RandIndex = rand() % 7;			//Random pull from index for name descriptor

	cout << "That is a " << adj[RandIndex] << " name, " << name << ". Well met!" << endl << endl;  //Index assigns random descriptor here. Sorry if you got 'stupid'.
	system("pause");
	system("cls");
	cout << "============================================================" << endl;				//Huge chunk of flavor text starts here
	cout << ". . .: ::~ ~: Chapter 1: The Adventure Begins! :~ ~:: :. . ." << endl;
	cout << "============================================================" << endl << endl;
	cout << "You walk out of the registration hall, a fully licensed adventurer with a signed" << endl;
	cout << "identification card in your pouch. The smell of salt in the air overwhelms you." << endl << endl;
	cout << "The seaside town of Ma-Mal is picturesque. The sun is bright, but the ocean breeze" << endl;
	cout << "is immensely kind--a cool wave of air washes through the top of your head. Your friend" << endl;
	cout << "was right about Ma-Mal Island--the people are nice, the weather is pleasant," << endl;
	cout << "and judging by bounties placed on the bulletin board, the monsters don't sound" << endl;
	cout << "all too strong. In fact, if you shouted at them loud enough, you could probably" << endl;
	cout << "scare them into changing their stats. It's the perfect place for a newbie adventurer." << endl << endl;
	cout << "Like you!" << endl << endl;
	cout << "You strap on your sword and buckler, and make your way to the beach." << endl << endl;
	system("pause");
	system("cls");
	cout << endl << endl << endl;
	cout << "The shoreline looks absolutely empty. You're having a few doubts about this town." << endl;
	cout << "It doesn't matter if the enemies are weak. You're not going to earn XP if there" << endl;
	cout << "aren't any enemies to fight anyway." << endl << endl;
	cout << "The only signs of life are streaks on the sand--trails of crabs heading into their homes." << endl << endl;
	cout << "Actually, now that you look at it, the streaks look somewhat erratic. Almost like" << endl;
	cout << "they were running from something. Is the beach empty because a bigger creature scared" << endl;
	cout << "them away?" << endl << endl;
	cout << "Just as you began to think that, the ground started to rumble . . ." << endl << endl;
	system("pause");
	system("cls");
	cout << endl << endl << endl;
	cout << "The waves begin to get stronger as something slowly emerges out of the water." << endl;
	cout << "The training you had in Novice Camp kicks in and you automatically draw your" << endl;
	cout << "sword out of its sheath. You stare at the water intently, waiting for the creature" << endl;
	cout << "to finally reveal itself." << endl << endl;
	cout << R"_("Lay on, MacDuff!")_" << endl << endl;
	system("pause");
	system("cls");

	//Generate Foe
	RandIndex = rand() % 8;		//Again, I did this only to avoid having long if/else if segments. I apologize if I googled advanced stuff.
	foeName = foe[RandIndex];

	if (name == "Snake")
	{
		foeName = "Metal Gear";
	}

	else if (name == "Link")
	{
		foeName = "Ganondorf";
	}

	else if (name == "Guybrush")
	{
		foeName = "LeChuck";
	}

	else if (name == "Jero")
	{
		foeName = "a bug in the game";
	}

	else if (name == "Sonic")
	{
		foeName = "Eggman";
	}

	else if (name == "Rockman")
	{
		foeName = "Dr. Wahwee";
	}

	else if (name == "Mario")
	{
		foeName = "Bowser";
	}

	else if (name == "Cloud")
	{
		foeName = "Sephiroth";
	}

	cout << endl << endl << endl;
	cout << "The surface of the water breaks!" << endl << endl;
	cout << "You are fighting " << foeName << "!" << endl << endl;

	//Unique Encounters - Introduction
	if (name == "Snake")
	{
		cout << "The Metal Gear Rising towards you makes you back up--it's like it's out for Revengeance. There's a Phantom" << endl;
		cout << "Pain that grips your chest, as the beach you're on turns into the Ground Zero for a massive battle. You" << endl;
		cout << "look at your sword a bit apprehensively and wish you had the Guns of the Patriots with you instead; although," << endl;
		cout << "it might not have mattered--you don't think there's a weapon to surpass the Metal Gear. Regardless," << endl;
		cout << "you are the boss. The Snake Eater. The nanomachines in your body start to change your health and" << endl;
		cout << "attack values. You're ready." << endl << endl;
	}

	else if (name == "Link")
	{
		cout << "The Thief King rises out of the water, his dark robes dragging trails in the sand. His right hand glows" << endl;
		cout << "for a second as he clenches it into a fist. You grip your Master Sword tightly. You're pretty sure you're" << endl;
		cout << "not supposed to have that sword this early in the game, but you decide not to question it. There is some" << endl;
		cout << "fear in you, but you still decide to face this battle head on. Just as you think that, your own left hand" << endl;
		cout << "glows. A triangle shows up on the back of your hand. The goddesses seem to have blessed you. You feel like" << endl;
		cout << "you might actually win this fight. With the fury of a thousand Cuccos, you shout at Ganondorf." << endl << endl;
		cout << R"_("Hyut HAH HIYAAAAAH!!")_" << endl << endl;
	}

	else if (name == "Guybrush")
	{
		cout << R"_(LeChuck's rotting corpse rises out as he walks to the shore. "Ah, Threepwood. You've no rootbeer)_" << endl;
		cout << R"_(to save you this time around!" You look at him and frown. The salty sea dog's been a pain in your)_" << endl;
		cout << "side for as long as he's been trying to get with your wife, but he's actually right this time. One look" << endl;
		cout << "at your inventory and it's plain to see that you don't have any bottles of rootbeer indeed. You try to bluff" << endl;
		cout << R"_(your way past him. "That's what you think, you monkey-faced buffoon." He smirks at you, "And me thinks that)_" << endl;
		cout << R"_(what you think that what I think is right.")_" << endl << endl;
		cout << "Dang. That was confusing. And he called your bluff." << endl << endl;
		cout << "You take the voodoo doll out of your inventory and stab your sword through it. LeChuck makes a weird face before" << endl;
		cout << "drawing his own cutlass. This is going to be a hard battle unless you change both the health and attack stats" << endl;
		cout << "for the both of you." << endl << endl;
	}

	
	else if (name == "Jero")
	{
		cout << "The bug comes up like a few red squiggly lines. It's not that hard to spot, but given your" << endl;
		cout << "limited C++ knowledge, you're not quite sure how to tackle this bug. Googling the answers is helping" << endl;
		cout << "a lot though, and so you're able to begin debugging as soon as you can. You decide to hack into its" << endl;
		cout << "health and attack power to make them lower, and also give yoursef a slight advantage." << endl;
	}

	else if (name == "Sonic")
	{
		cout << "Like a blue blur, you zip past a preemptive strike towards you and you spinball jump" << endl;
		cout << "into a counter attack, but Dr.Eggman was ready. A robo-tentacle reaches out from behind him and" << endl;
		cout << "it throws you aside towards a tree. There is a defeaning chime as a hundred rings simultaneously" << endl;
		cout << "hit the ground. You can feel pain and can hardly stand. You're unable to recover any of your" << endl;
		cout << "rings before they disappear. On the robo-octopus, Dr. Eggman makes his way slowly towards you." << endl << endl;
		cout << "SLOWLY." << endl << endl;
		cout << "No, no, no. Slow won't do. You feel your insides churning. You let out a scream as you charge towards" << endl;
		cout << "the massive robot. You gotta go fast. So fast, in fact, that the sound barrier breaks and" << endl;
		cout << "both your health and attacks were changed." << endl << endl;
	}

	else if (name == "Rockman")
	{
		cout << "The UFO-like glider floats out of the ocean, carrying a laughing man who vaguely resembles" << endl;
		cout << "Albert Einstein in a lab coat. You hear a voice come up like a radio in your ears," << endl << endl;
		cout << R"_("Wockman! You must defeat Doctah Wahwee!" You're not quite sure what that was all about,)_" << endl;
		cout << "but all the same, you feel like you have to defeat Dr. Wil--I mean, Dr. Wahwee. You aim" << endl;
		cout << "your Mega Buster Sword at the craft and shoot lemons at it. The ship fires back a shot at you," << endl;
		cout << "barely missing you. The laser shot creates a massive crater behind you, revealing an" << endl;
		cout << "energy tank. This is perfect. Time for phase 2 of this battle. . ." << endl << endl;
	}

	else if (name == "Mario")
	{
		cout << "The familiar puttering of the Koopa Copter can be heard as the looming shadow of" << endl;
		cout << "the mean, green-shelled fiend approaches you. He laughs menacingly. You swear" << endl;
		cout << "that there was smoke coming out of his mouth. You're pretty sure he can" << endl;
		cout << "breathe fire. You grip your sword tighter and get ready to attack." << endl;
		cout << R"_(Bowser sees the sword and his reaction immeiately changes. "Hang on. You)_" << endl;
		cout << R"_(can't have a sword! It's usually a Fire Flower, or a shell, or at the most a Hammer." )_" << endl << endl;
		cout << R"_(You shake your head, smiling. "Haven't you heard of the RPG, Legend of the Seven Stars?")_" << endl << endl;
		cout << R"_(Wait! That game's a spinoff! That's not canon!)_" << endl << endl;
		cout << "You start laughing. You've been in so many titles to know that it doesn't matter whether" << endl;
		cout << "or not something is a spinoff. Mario RPG is a beloved title and so it's practically accepted" << endl;
		cout << "in canon anyway. The fanbase can't all be wrong. If the fans wanted to dictate health" << endl;
		cout << "and attack stats, they might probably get away with it. You shout at Bowser as you" << endl;
		cout << "charge forward and take a few liberties with certain values of the game." << endl << endl;
	}

	else if (name == "Cloud")
	{
		cout << "Sephiroth's body rises high, a single wing emerging dramatically behind him. Really" << endl;
		cout << "ominous music starts playing, accompanied by a Gregorian chant. You can't make out" << endl;
		cout << "what they're saying, but it's probably Latin. You can vaguely hear Sephiroth mumble" << endl;
		cout << "something about a philosophical ideal about his mother or something. All you know is" << endl;
		cout << "that he stabbed your girlfriend, and now you want to stab him back. With materia slots full," << endl;
		cout << "you raise your Buster Sword, and rush at the long-haired freak. You're a silent" << endl;
		cout << "protagonist so you don't shout or anything, but the sheer force of your running" << endl;
		cout << "causes a few changes to happen" << endl << endl;
	}

	else
	{
		cout << "For a second you're stunned at seeing its ominous form rising from the sea," << endl;
		cout << "but you remember that you can scare the creature into changing its stats" << endl;
		cout << "like health and attack. You're actually feeling a bit more confident after" << endl;
		cout << "remembering that, so you can probably change your own health too, if you wanted." << endl << endl;
		system("pause");
		system("cls");
		cout << endl << endl << endl;
		cout << "You take a deep breath and scream as loud as you can at the creature!" << endl;
		cout << R"_("I am )_" << name << R"_(! I am a Novice Adventurer! AND I WILL DESTROY YOU!")_" << endl << endl;
	}

	//Initializing Values
	while (HPmax <= 0 || foeHP <= 0)
	{
		cout << "Your HP is ";
		cin >> HPmax;

		HP = HPmax;		//HPmax is replaced by HP to track current health; HPmax now retains value for dynamic battle text to work

	
	PlayerATK:									//Sorry, sir. I had to learn to use labels; otherwise, I would have a 3-level nest here D:
		cout << "Your attack values are" << endl << "Minimum Damage : ";
		cin >> Min;
		cout << "Maximum Damage: ";
		cin >> Max;

		if (Min > Max)
		{
			cout << "Your Minimum Attack value can't be lower than the Maximum!" << endl;
			cout << "Try again." << endl << endl;
			system("pause");
			system("cls");
			cout << endl << endl << endl;
			goto PlayerATK;
		}

		else if (Min == Max)
		{
			cout << "Your Minimum and Maximum Attack value can't be the same!" << endl;
			cout << "Try again." << endl << endl;
			system("pause");
			system("cls");
			cout << endl << endl << endl;
			goto PlayerATK;
		}

		cout << endl << "The enemy's HP is ";
		cin >> foeHPmax;

		foeHP = foeHPmax;		//foeHPmax is replaced by foeHP to track current health; foeHPmax now retains value for dynamic battle text to work

	FoeATK:
		cout << "Its attack values are" << endl << "Enemy Minimum Damage: ";
		cin >> foeMin;
		cout << "Enemy Maximum Damage: ";
		cin >> foeMax;

		if (foeMin > foeMax)
		{
			cout << "The enemy's Minimum Attack value can't be lower than the Maximum!" << endl;
			cout << "Try again." << endl << endl;
			system("pause");
			system("cls");
			cout << endl << endl << endl;
			goto FoeATK;
		}

		else if (foeMin == foeMax)
		{
			cout << "The enemy's Minimum and Maximum Attack value can't be the same!" << endl;
			cout << "Try again." << endl << endl;
			system("pause");
			system("cls");
			cout << endl << endl << endl;
			goto FoeATK;
		}

		cout << endl;
		system("pause");
		system("cls");

		if (HP <= 0 || foeHP <= 0)
		{
			cout << endl << endl << endl;
			cout << "Well... one of you is already dead, so I'll need you to do that again." << endl;
			system("pause");
			system("cls");
			cout << endl << endl << endl;
		}

		cout << endl << endl << endl;
		cout << name << "'s HP: " << HP << "  Minimum Attack: " << Min << "  Maximum Attack: " << Max << endl;
		cout << "Enemy HP: " << foeHP << "  Minimum Attack: " << foeMin << "  Maximum Attack: " << foeMax << endl << endl;
		system("pause");
		system("cls");
		cout << endl << endl << endl;
	}

	LoopFight:
	system("cls");
	cout << "==============================" << endl;					//More stylized headers
	cout << "== ..:: BATTLE  START! ::.. ==" << endl;
	cout << "==============================" << endl << endl;

	//Fight Loop
	while (HP > 0 && foeHP > 0)
	{
		//Inclusive Range Value Randomization
		Dmg = rand() % (Max - Min + 1) + Min;
		foeDmg = rand() % (foeMax - foeMin + 1) + foeMin;		//Included in loop so that the randomized attack value changes every fight instance

		if (HP == HPmax)		//Dynamic battle text; changes dependent on player and monster HP
		{
			cout << "You and the monster stare at each other. The sands shift beneath you as you ready your blade." << endl;
			cout << "The enemy prepares itself, charging its own attack, rearing its ugly head and letting out a tiny," << endl;
			cout << R"_("meep!" You're not quite sure how to react to that one, but you decide not to get distracted.)_" << endl << endl;
		}

		else if (HP >= (HPmax / 2))
		{
			cout << "You run towards the beast!" << endl << endl;
		}

		else if (HP <= (HPmax / 2) && HP > (HPmax / 5))
		{
			cout << "Your breathing gets a little haggard. The battle is taking its toll on you." << endl << endl;
		}

		else if (HP <= (HPmax / 5) && HP > 0)
		{
			cout << "You feel the warmth of blood dripping from your forehead. It's getting in your eyes." << endl;
			cout << "How is a runt like this beating you? Aren't its actions randomized anyway? It's not" << endl;
			cout << "like it can intelligently decide what to do next . . ." << endl << endl;
		}

		if (foeHP > (foeHPmax / 2))
		{
			cout << "The enemy rushes towards you!" << endl << endl;
		}

		else if (foeHP <= (foeHPmax / 2) && foeHP > (foeHPmax / 5))
		{
			cout << "The enemy seems a little slower. Whatever you're doing, keep doing it. It seems to be working!" << endl << endl;
		}

		else if (foeHP <= (foeHPmax / 5) && foeHP > 0)
		{
			cout << "You can hear its heavy breathing though. It's moving a lot slower, but that's not enough." << endl;
			cout << "This monster isn't going to run away. You need to end this soon." << endl << endl;
		}

		cout << "(a)  Attack" << endl << "(d)  Defend" << endl << "(w)  Wild Attack" << endl << endl;
		cout << "What do you do?  ";
		cin >> Atk;
		system("cls");

		//Enemy AI
		foeAI = rand() % 3 + 1;			//I can actually do this with an array .

		if (foeAI == 1)
		{
			foeAtk = "a";
		}

		else if (foeAI == 2)
		{
			foeAtk = "d";
		}

		else if (foeAI == 3)
		{
			foeAtk = "w";
		}

		//Match Up Determination
		if (Atk == "a" && foeAtk == "a")
		{
			HP = HP - foeDmg;
			foeHP = foeHP - Dmg;

			cout << endl << endl << endl;
			cout << "Both of you rush at each other and attack at the same time!" << endl;
			cout << "Your sword hits it on the head and you deal " << Dmg << " damage to it!" << endl;
			cout << "It lands a blow on you as well, and you take " << foeDmg << " damage!" << endl << endl;
		}

		else if (Atk == "a" && foeAtk == "d")
		{
			Dmg = Dmg / 2;
			foeHP = foeHP - Dmg;

			cout << endl << endl << endl;
			cout << "You swing at the " << foeName << ", but it was able to curl up and block it" << endl;
			cout << "in time! It only took " << Dmg << " damage that time." << endl << endl;
		}

		else if (Atk == "a" && foeAtk == "w")
		{
			foeDmg = foeDmg * 2;
			HP = HP - foeDmg;
			foeHP = foeHP - Dmg;

			cout << endl << endl << endl;
			cout << "Running up to the enemy, you swing at it causing " << Dmg << " to it. Unfortunately," << endl;
			cout << "the attack left you open, and the opponent got you with a wild attack, doing " << foeDmg << " damage." << endl << endl;
		}

		else if (Atk == "d" && foeAtk == "w")
		{
			Dmg = Dmg * 2;
			foeHP = foeHP - Dmg;

			cout << endl << endl << endl;
			cout << "It charges wildly at you! It's a good thing you raised your buckler, because the attack" << endl;
			cout << "hardly did anything except stagger the opponent. Before it could recover, you counter" << endl;
			cout << "with a critical strike! The enemy takes " << Dmg << " to its health!!" << endl << endl;
		}

		else if (Atk == "d" && foeAtk == "d")
		{
			cout << endl << endl << endl;
			cout << "You raise your buckler up ready to block an attack, but when you look at the enemy" << endl;
			cout << "it seems to be curling up defensively. It seems like both of you expected the other" << endl;
			cout << "to attack. Nothing happened this time." << endl << endl;
		}

		else if (Atk == "w" && foeAtk == "w")
		{
			Dmg = Dmg * 2;
			foeDmg = foeDmg * 2;
			HP = HP - foeDmg;
			foeHP = foeHP - Dmg;

			cout << endl << endl << endl;
			cout << "Both of you wildly flail at each other! It's a messy attack leaving both of you open, so" << endl;
			cout << "both you and the opponent pretty much took a lot of damage that time around. You took " << foeDmg << endl;
			cout << "while it took " << Dmg << " points of damage." << endl << endl;
		}

		else if (Atk == "d" && foeAtk == "a")
		{
			foeDmg = foeDmg / 2;
			HP = HP - foeDmg;

			cout << endl << endl << endl;
			cout << "You raise your buckler up just in time to block an incoming strike! It was close but you" << endl;
			cout << "managed to block most of the damage, taking only " << foeDmg << " damage this round." << endl << endl;
		}

		else if (Atk == "w" && foeAtk == "a")
		{
			Dmg = Dmg * 2;
			HP = HP - foeDmg;
			foeHP = foeHP - Dmg;

			cout << endl << endl << endl;
			cout << "You jump up and swing your sword straight down at the enemy! The enemy tries to catch you in" << endl;
			cout << "midair, but it couldn't stop your sword from making the sickest slash over its head. It" << endl;
			cout << "took " << Dmg << " while you only took " << foeDmg << " damage. You take a few steps back and" << endl;
			cout << "swish your sword really quickly to the side making the blood fly off of the blade." << endl;
			cout << "You almost look cool." << endl << endl;
		}

		else if (Atk == "w" && foeAtk == "d")
		{
			foeDmg = foeDmg * 2;
			HP = HP - foeDmg;

			cout << endl << endl << endl;
			cout << "You take a running jump at your opponent and try to swing your sword down wildly" << endl;
			cout << "but before you can land a hit, your opponent curls into a ball and completely blocks" << endl;
			cout << "your attack! You fall awkwardly back to the sand. Seeing its chance, the foe grabs you by" << endl;
			cout << "the ankle and swings you down like a wet rag. You took a heavy " << foeDmg << " damage from that attack." << endl << endl;
		}

		else
		{
			cout << endl << endl << endl;
			cout << "That's not a choice. Try again." << endl;
		}

		system("pause");
		system("cls");
		cout << endl << "HP:  " << HP << R"_(         ")_" << foeName << R"_(" HP:)_" << foeHP << endl << endl;
	}

	system("pause");
	system("cls");

	//Win/Loss Conditions
	if (name == "Snake" && HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "Snake?" << endl << endl << "Snake?!!" << endl << endl << "SNAAAAAAAKKKEEEE!!!" << endl << endl;
		cout << R"_(  _________  ___      __    __  ____    _____  _  _  ____  ___ )_" << endl;
		cout << R"_( / _______/ /___\    |__\  /__||____|  |_____||_||_||____||_  \)_" << endl;
		cout << R"_(/ /  ____  _______    ___  ___  ____    _   _  _  _  ____  _| /)_" << endl;
		cout << R"_(\ \ /_  / /  ___  \  |   \/   ||  __|  | | | || || ||  __||   \)_" << endl;
		cout << R"_( \ \_/ / /  /   \  \ | |\  /| || |__   | |_| || \/ || |__ | |\ \)_" << endl;
		cout << R"_(  \_  / /__/     \__\|_| \/ |_||____|  |_____| \__/ |____||_| \ \)_" << endl;
		cout << R"_(   / / _______________________________________________________ \ \)_" << endl;
		cout << R"_(  /_/ /_______________________________________________________\ \_\)_" << endl << endl;
	}

	else if (name == "Link" && HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "Ganondorf looms over your frail body. The light on his right hand begins to glow brighter as" << endl;
		cout << "your own light fades. He now has two pieces of the Triforce. With you down, there's nothing to" << endl;
		cout << "stop him from taking the last piece from the princess. You only hope that she does something to at" << endl;
		cout << "least slow him down, like breaking it into eight pieces and scattering them among eight different" << endl;
		cout << "dungeons. The Master Sword by your side glows just a little before it too fades. The old man was right." << endl << endl;
		cout << "It's dangerous to go alone." << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "~ ~ . . : : GAME OVER : : . . ~ ~" << endl;
		cout << "---------------------------------" << endl << endl;
	}

	else if (name == "Guybrush" && HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "Your weak body collapses to the ground and lies motionless. You can't feel much of anything, actually." << endl;
		cout << "LeChuck picks you up and straps you on a death machine. You hang helpless over a large pit of acid," << endl;
		cout << "up until the mechanism keeping you up suddenly activates and you are slowly lowered into the acid." << endl << endl;
		system("pause");
		system("cls");
		cout << endl << endl << endl;
		cout << "Okay, okay. I know it sounds like a lot of horse hockey. I don't expect you to believe you were" << endl;
		cout << R"_(disintegrated in acid. And you might be thinking, "Well, sure I--" And I'm gonna stop you right there.)_" << endl;
		cout << "You're there right now, playing this game, looking very integrated." << endl << endl;
		cout << "OKAY, so I embellished it a little for dramatic effect. Sue me." << endl;
		cout << "Anyway, what REALLY happened was you were hanging over a pit of acid. Death was so close you could" << endl;
		cout << "smell his hariy armpits. . ." << endl << endl;
		system("pause");
		system("cls");
		cout << endl << endl << endl;
		cout << "You know what. It'd be a lot easier if you just restarted the game." << endl << endl;
		HP = HPmax;
		goto LoopFight;
	}

	else if (name == "Jero" && HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "The sword drops from your hands. The bug is getting worse, and it's showing no signs of slowing down." << endl;
		cout << "You've googled as much as you can, and dowloaded as much freeware that you think you need just" << endl;
		cout << "to troubleshoot this bug, but to no avail. Now, it's gotten worse. Every time you compile the script," << endl;
		cout << "Windows Defender keeps reading the .exe as carrying a Trojan virus. You're panicking already." << endl;
		cout << "Your friend was right. There might be too much flavor text." << endl << endl;
		system("pause");
		system("cls");
		cout << endl << endl << endl;
		cout << "It's evolving . . . The .exe now crashes. Your computer freezes every so often, and you've" << endl;
		cout << "started to back up most of your files. You open up Visual Studio and wait at least 10 minutes for" << endl;
		cout << "everything to load before tearing through more than 600 lines of code just to find the bug. It's gonna" << endl;
		cout << "be a long night. Again." << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "~ ~ . . : : GAME OVER : : . . ~ ~" << endl;
		cout << "---------------------------------" << endl << endl;
	}

	else if (name == "Sonic" && HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "All your rings are spent, and nothing can shield you from the final blow of Dr. Eggman's robo-octopus." << endl;
		cout << "You gotta go fast, but you can't. You just hope that Knuckles was able to hide all the Chaos Emeralds in time." << endl << endl;
		cout << "You're pretty sure not even a kiss from Princess Elise can save you now." << endl << endl;
		cout << "As your vision fades completely, a song starts to play you out. You hear the words of the song, and" << endl;
		cout << "you imagine this is what heaven probably feels like: endless running, a killer soundtrack," << endl << endl;
		cout << "an escape from the city." << endl << endl;
		system("pause");
		system("cls");
		cout << endl << endl << endl;
		cout << "						Rolling around at the speed of sound" << endl;
		cout << "					  Got places to go, gotta follow my rainbow" << endl;
		cout << "					  Can't stick around, have to keep movin' on" << endl;
		cout << "					Guess what lies ahead, only one way to find out" << endl << endl;
		cout << "								Must keep on movin' ahead" << endl;
		cout << "						No time for guessin', follow my plan instead" << endl;
		cout << "							  Trusting in what you can't see" << endl;
		cout << "							  Take my lead, I'll set you free" << endl << endl;
		cout << "							 Follow me - set me free - trust me" << endl;
		cout << "							  And we will escape from the city" << endl;
		cout << "								I'll make it through, follow" << endl;
		cout << "							 Follow me - set me free - trust me" << endl;
		cout << "							  And we will escape from the city" << endl;
		cout << "							I'll make it through, prove it to you" << endl;
		cout << "										   Follow me" << endl << endl;
		cout << "							 Danger is lurking around every turn" << endl;
		cout << "						  Trust your feelings, gotta live and learn" << endl;
		cout << "						I know with some luck that I'll make it through" << endl;
		cout << "							Got no other options, only one thing to do" << endl << endl;
		system("pause");
		system("cls");
		cout << endl << endl << endl;
		cout << "								  I don't care what lies ahead" << endl;
		cout << "						 No time for guessin', follow my plan instead" << endl;
		cout << "						Find that next stage no matter what that may be" << endl;
		cout << "								Take my lead, I'll set you free" << endl << endl;
		cout << "							   Follow me - set me free - trust me" << endl;
		cout << "							    And we will escape from the city" << endl;
		cout << "								  I'll make it through, follow" << endl;
		cout << "							   Follow me - set me free - trust me" << endl;
		cout << "								And we will escape from the city" << endl;
		cout << "							 I'll make it through, prove it to you" << endl;
		cout << "											Follow me" << endl << endl;
		cout << "											Follow me" << endl;
		cout << "									   I'll make it through" << endl << endl;
		cout << "											Oh, yeah" << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "~ ~ . . : : GAME OVER : : . . ~ ~" << endl;
		cout << "---------------------------------" << endl << endl;

	}

	else if (name == "Rockman" && HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "You jump, jump, slide, and slide past all of Dr. Wahwee's laser shots. But it's no use." << endl;
		cout << "He's too fast, and you haven't yet fully memorized the attack pattern. It's almost as bad as" << endl;
		cout << "that time with Yellow Devil. It's too late now, a laser shot barely grazed your ankle that time." << endl;
		cout << "You can't believe that hit--it's almost as bad as Mighty No. 9." << endl << endl;
		cout << "You freeze in midair before exploding into several smaller orbs of light." << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "~ ~ . . : : GAME OVER : : . . ~ ~" << endl;
		cout << "---------------------------------" << endl << endl;
	}

	else if (name == "Mario" && HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "Bowser shoots a fireball at you, but you aren't able to deflect it in time. The resulting explosion" << endl;
		cout << "leaves you smaller and slower. You can't break blocks now, but all you need to do is jump up high" << endl;
		cout << "enough to land on his head. And you're sure you can do it. They call you Jumpman, after all." << endl << endl;
		cout << "But today really isn't your day. You jump up as high as you can, swinging your sword up ready to bring it" << endl;
		cout << "down on his head, but he grabs you by the ankles and starts to spin you around. Once he gets spinning" << endl;
		cout << "fast enough, he lets go and you fly head-first into a boulder. There is a sick CRACK as you realize" << endl;
		cout << "you're never going to be able to jump again. You're going to spend your days in physical therapy, never" << endl;
		cout << "getting better, wasting your days playing Mario Party 3 with whoever you can manage to rope in." << endl << endl;
		cout << "A low, rumbling, slow laugh is heard as you do a flip in the air and tumble into the off-screen abyss." << endl;
		cout << "You can barely make out a whispered taunt escape from between his pointed teeth." << endl << endl;
		cout << R"_("The princess was NEVER in this castle!")_" << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "~ ~ . . : : GAME OVER : : . . ~ ~" << endl;
		cout << "---------------------------------" << endl << endl;
	}

	else if (name == "Cloud" && HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "The Gregorian chant climaxes as Sephiroth swoops down and stabs you in one fluid motion. There is no fanfare." << endl;
		cout << "There are no more Phoenix Downs. No more last second saves. All there is is darkness." << endl;
		cout << "Darkness--then a torn film reel appears, the image of Shinra Inc.printed on every frame." << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "~ ~ . . : : GAME OVER : : . . ~ ~" << endl;
		cout << "---------------------------------" << endl << endl;
	}

	else if (HP <= 0 && foeHP <= 0)
	{
		cout << endl << endl << endl;
		cout << "Both you and the monster look at each other intensely. This was not how it was supposed to end." << endl;
		cout << "The monster sways left and right before coming down to the sand with a THUD. Your grip on your" << endl;
		cout << "sword loosens and it falls to the ground. You can't feel your hands. You can't feel your legs" << endl;
		cout << "either. In fact, you can't feel anything at all. If anything, you feel pain." << endl << endl;
		cout << "A dark shadow materializes over the dead body of the monster. A thin bone-like arm reaches" << endl;
		cout << "down and touches the body. It then turns and walks towards you. You have a hard time keeping" << endl;
		cout << "your head up, but you can clearly see who is making its way towards you." << endl << endl;
		cout << R"_("Get away from me you evil--")_" << endl << endl;
		cout << "HEY, NOW. THAT'S NOT TOO NICE. I'M DOING DOUBLE DUTY ON THE JOB HERE." << endl << endl;
		cout << R"_(There are tears streaming down your face as you bargain with him, "You can't take my soul.)_" << endl;
		cout << R"_(I still have many adventures I need to go on!")_" << endl << endl;
		cout << "BUT THAT DOESN'T REALLY CHANGE ANYTHING NOW, DOES IT?" << endl << endl;
		cout << R"_("No," you feebly reply. "I suppose it doesn't.")_" << endl << endl;
		system("pause");
		system("cls");
		cout << endl << endl << endl;
		cout << "THAT'S RIGHT. ALL THAT MATTERS IS YOU'RE DEAD NOW, AND I'M HERE TO COLLECT YOUR SOUL." << endl << endl;
		cout << "His scythe comes out of nowhere and slices into you. You don't feel anything except cold air run through" << endl;
		cout << "your stomach like a breeze. Right before you slip away, you think about how that was the fastest cut" << endl;
		cout << "you've ever seen. Maybe if you could cut like that, you'd still be alive now. You half wish that Death" << endl;
		cout << "would teach you in the afterlife how to do that move. But you know he won't." << endl;
		cout << "You close your eyes and let yourself rest forever." << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "~ ~ . . : : GAME OVER : : . . ~ ~" << endl;
		cout << "---------------------------------" << endl << endl;
	}

	else if (HP <= 0 && foeHP > 0)
	{
		cout << endl << endl << endl;
		cout << "You've taken more hits than you can and you get really woozy." << endl;
		cout << "You fall to your knees, trying to keep yourself up with the sword, but it's no use. Your body" << endl;
		cout << "just keeps getting heavier. You can hear the hushed sound of the waves washing back and forth over" << endl;
		cout << "the shoreline. The sands slowly shift as your enemy slowly makes its way towards you--no doubt it will" << endl;
		cout << "want to finish you off." << endl << endl;
		cout << "Just before your vision fades, you see a shadow looming behind the enemy. It too is walking towards you." << endl;
		cout << "He smiles as he notices you looking at him. When he reaches you, he carefully lifts his scythe above your head." << endl << endl;
		cout << "TIME'S UP, " << name << ". YOU DIED TO SUCH AN INSIGNIFICANT CREATURE." << endl;
		cout << "TO BE FAIR, YOU'RE NOT ALL TOO SIGNIFICANT TO BEGIN WITH, SO IT" << endl;
		cout << "SHOULDN'T BE THAT BAD A LOSS. YOU CAN PROBABLY RESTART THE GAME ANYWAY." << endl << endl;
		cout << R"_("I have no idea what you're talking about . . .")_" << endl << endl;
		cout << "THAT'S OKAY. IT'S QUIET TIME NOW." << endl << endl;
		cout << "The scythe comes down quick and almost painless, like a kiss." << endl << "The monster begins to eat your flesh." << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "~ ~ . . : : GAME OVER : : . . ~ ~" << endl;
		cout << "---------------------------------" << endl << endl;
	}

	else if (foeHP <= 0 && HP > 0)
	{
		cout << endl << endl << endl;
		cout << "There is relief in your breath as the creature spins its last and collapses to the sand." << endl;
		cout << "You fall to your butt, more out of exhaustion than anything. You let the cool sea breeze wash" << endl;
		cout << "over you for a while. After a few minutes, you begin wiping the blood and sand off of your" << endl;
		cout << "sword. It pays to keep your blade clean--it keeps the edge sharp. And you'll need it to stay" << endl;
		cout << "sharp. This was only your first monster. You might have defeated " << foeName << ", but" << endl;
		cout << "this is only the start of your journey as an adventurer. You have many more fights" << endl;
		cout << "with stronger creatures. You have dozens of other places to visit. No, you cannot be resting" << endl;
		cout << "now, not now of all times." << endl << endl << "You stand up and slide your sword into its sheathe. You readjust the" << endl;
		cout << "buckler on your arm. You pat down your clothes and straighten them out. You're ready, you think to yourself." << endl << endl;
		cout << R"_("RUMBLE")_" << endl << endl;
		cout << "The ground begins to shift beneath you again. The now familiar feeling of a random encounter starts anew." << endl;
		cout << R"_(You look up to the waters and smile. "I'm ready.")_" << endl << endl;
		cout << "---------------------------------" << endl;
		cout << "=~ ..:: CONGRATULATIONS!! ::.. ~=" << endl;
		cout << "---------------------------------" << endl << endl;
	}

	system("pause");
	return 0;
}